# JS preload tests


This repo tests how a browser behaves when using the tag <link rel="preload" /> for javascript resources.


## Tests

3 JS files:

* One that waits for DomContentLoaded native event
* One self executing function
* One big/slow script (which can be transformed into N scripts as it responds with a different script via query params)

Run the servers
```sh
# serving the big/slow resources
node server.js

# serving the tests
python -m SimpleHTTPServer
```

### 1. Dom Content Loaded (t1-setup-dom-loaded.html)

How does the browser behave with a preload link in the head tag and the script tag at the end of the body tag with a script that executes after the dom content load event is fired.


### 2. Right away (t2-setup-right-away.html)

How does the browser behave with a preload link in the head tag and the script tag at the end of the body tag with a script that executes right away.


### 3. One big/slow file at the begining (t3-one-big-slow-file-begining.html)

How does the browser behave with preload links in the head tag and the script tags at the end of the body tag with scripts that execute after the dom content loaded event is fired and right away. The big/slow script is set as the first link to be preloaded and as the first script tag.


### 4. One big/slow file at the begining (t4-one-big-slow-file-begining-async.html)

How does the browser behave with preload links in the head tag and the script tags (with async attribute) at the end of the body tag with scripts that execute after the dom content loaded event is fired and right away. The big/slow script is set as the first link to be preloaded and as the first script tag.


### 5. One big/slow file at the end (t5-one-big-slow-file-end.html)

How does the browser behave with preload links in the head tag and the script tags at the end of the body tag with scripts that execute after the dom content loaded event is fired and right away. The big/slow script is set as the last link to be preloaded and as the last script tag.


### 6. One big/slow file at the end (t6-one-big-slow-file-begining-async.html)

How does the browser behave with preload links in the head tag and the script tags (with async attribute) at the end of the body tag with scripts that execute after the dom content loaded event is fired and right away. The big/slow script is set as the last link to be preloaded and as the last script tag.


### 7. Several big/slow files at the begining (t7-several-big-slow-files-begining.html)

How does the browser behave with preload links in the head tag and the script tags at the end of the body tag with scripts that execute after the dom content loaded event is fired and right away. The big/slow scripts are set as the first links to be preloaded and as the first script tags.


### 8. Several big/slow files at the end (t8-several-big-slow-files-end.html)

How does the browser behave with preload links in the head tag and the script tags at the end of the body tag with scripts that execute after the dom content loaded event is fired and right away. The big/slow scripts are set as the last links to be preloaded and as the last script tags.


## Conclusions

- Tests #1 and #2 behave as intended, script is preloaded and then executed.
- Tests #3 and #7 demonstrate that if big/slow scripts are set as the first script tags then the rest of the page will have to wait for these resources to finish loading (while still benefitting from the preload stage).
- Tests #4, #5, #6 and #8 demonstrate how a self executing script (set before the big/slow script resource) can take ownership of the page and execute some actions while the rest of the scripts finish loading.
- Tests #7 and #8 demonstrate how the browser respects the limit of scripts to be loaded in parallel and how the overall load time benefits from both preloading and parallel loading.

### Notes

- This is not an actual benchmark but a way to test and see how this tool behaves. Feel free to test with your own setup(s) and contribute with your conclusions/results.
