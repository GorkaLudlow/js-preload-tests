const http = require('http')
const fs = require('fs')
const path = require('path')
const PORT = 8125
const wait = 5000

http.createServer(function (req, res) {
  console.log('Received request', req.headers)
  setTimeout(() => {
    console.log('Sent response')
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/javascript');
    res.end(`console.log('Script executed after a ${wait} milisecond load'); console.log('${JSON.stringify(req.url)}')`);
  }, wait)
}).listen(PORT)

console.log(`Server running at http://127.0.0.1:${PORT}/`)

